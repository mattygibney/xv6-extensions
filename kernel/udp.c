#include "types.h"
#include "riscv.h"
#include "defs.h"
#include "lwip/opt.h"
#include "lwip/udp.h"

#define UDP_PORT 7

uint64 packet_count =0;
uint64 connection_time_udp=0;
uint64 time;
static void udpserver_raw_recv(void *arg, struct udp_pcb *pcb, struct pbuf *p, struct ip_addr *addr, u16_t port){
	LWIP_UNUSED_ARG(arg);
	packet_count++;
	if(packet_count % 1000 ==0){
		printf("UDP packets recevied: %d \n", packet_count);
		printf("UDP packets sent: %d \n", packet_count);
	}
	udp_sendto(pcb,p,addr,port);
	pbuf_free(p);
}
void udp_start_listening(struct ip_addr *IP_ADD){
	printf("Starting UDP server on port %d \n", UDP_PORT);
	struct udp_pcb *u_pcb;
	u_pcb = udp_new();
	
	
	udp_bind(u_pcb, IP_ADD, UDP_PORT);
	udp_recv(u_pcb, udpserver_raw_recv, u_pcb);

}


