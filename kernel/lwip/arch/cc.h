#define LWIP_PLATFORM_DIAG(x) do { printf x; } while(0)
#define	LWIP_NOASSERT 1

#define LWIP_PLATFORM_ASSERT 
#define LWIP_NO_CTYPE_H 1
#define LWIP_NO_UNISTD_H 1
#define LWIP_NO_INTTYPES_H 1
#define LWIP_NO_STDINT_H 1
#define LWIP_NO_CTYPE_H 1


#define X8_F "x"
#define S16_F "d"
#define U16_F "d"
#define X16_F "x"
#define S32_F "d"
#define U32_F "d"
#define X32_F "x"
#define SZT_F "d"

typedef unsigned char     u8_t;
typedef char     s8_t;
typedef unsigned short int    u16_t;
typedef short int     s16_t;
typedef unsigned int    u32_t;
typedef int     s32_t;

typedef unsigned long int uintptr_t;
typedef unsigned long int mem_ptr_t;

void printf(char *, ...);
#define LWIP_RAND r_mtime

typedef long ssize_t;

unsigned long r_mtime(void);

