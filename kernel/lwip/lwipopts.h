//Callback api only
#define NO_SYS 				 1
#define SYS_LIGHTWEIGHT_PROT 0

#define LWIP_NETCONN 0
#define LWIP_SOCKET  0

#define MEM_ALIGNMENT 		 4
#define MEM_SIZE 			 20000000
#define PBUF_BUF_SIZE 		 1600 
#define MEMP_NUM_PBUF 		 227 
#define MEMP_NUM_SYS_TIMEOUT 16

#define LWIP_IPV4 		1
#define LWIP_ARP 		1
#define LWIP_DHCP 		1
#define LWIP_UDP 		1
#define LWIP_TCP 		1
#define LWIP_DNS 		0
#define LWIP_ETHERNET 	1
#define LWIP_PERF 		1

#define CHECKSUM_CHECK_IP 	1
#define CHECSUM_CHECK_UDP 	1

#define LWIP_NETIF_LOOPBACK 1

//DEBUGGING OPTIONS
#define LWIP_DEBUG 0
//#define TCP_DEBUG LWIP_DBG_ON
//#define DHCP_DEBUG LWIP_DBG_ON
//#define NETIF_DEBUG LWIP_DBG_ON
//#define ETHARP_DEBUG LWIP_DBG_ON
//#define UDP_DEBUG LWIP_DBG_ON

//Stats options
#define LWIP_STATS_DISPLAY  0
#define LWIP_STATS          0
#define LWIP_TCP_STATS 		0
#define LWIP_MEM_STATS 		0
#define LWIP_MEMP_STATS 	0

#define PBUF_POOL_SIZE 			 10000
#define TCP_MSS					 1460
#define TCP_WND				 	 65535
#define TCP_SEND_BUF			 TCP_WND
#define TCP_SND_QUEUELEN		 16
#define MEMP_NUM_RAW_PCB 		 5
#define MEMP_NUM_TCP_PCB 		 15
