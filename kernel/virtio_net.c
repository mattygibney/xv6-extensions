//Qemu driver for the virtio-net mmio network driver

//variable types
#include "types.h"
#include "riscv.h"
#include "defs.h"
//Virtio addresses
#include "memlayout.h"
//virtio register offsets
#include "virtio.h"


//Address translation for virtio registers
#define R(r) ((volatile uint32 *)(VIRTIO1+(r)))


//Virtio receive virtqueue
static struct virt_queue_net receiveq;
//Virtio transmit virtqueue
static struct virt_queue_net transmitq;
//Virtio control virtqueu
static struct virt_queue_net controlq;

//List of buffers
struct virt_buffer receive_buffers[VIRTIO_NET_QUEUE_SIZE];
struct virt_buffer transmit_buffers[VIRTIO_NET_QUEUE_SIZE];


int recv_packet_counter=0;
//Packet buffer_index
int buffer_index=0;


//Zero buffers
void alloc_buffers(){
	for(int i=0;i<VIRTIO_NET_QUEUE_SIZE;i++){
		memset(transmit_buffers[i].buf,0,BUF_SIZE);
		transmit_buffers[i].used = 0;
		memset(receive_buffers[i].buf[i],0,BUF_SIZE);
		receive_buffers[i].used=0;
	}
	
}

int
virtio_net_init(int n,void *mac){
	//Status for driver to send to device
    uint32 status = 0;
	
	uint8 *mac_addr = (uint8 *)mac;
    
    //struct config device_config;
    struct config {
	    uint32 mac[6];
	    uint16 status;
    } virtio_net_config;

    //Check the virtio-device exists0
    if(*R(VIRTIO_MMIO_MAGIC_VALUE) != 0x74726976 ||
       *R(VIRTIO_MMIO_VERSION) != 1 ||
       *R(VIRTIO_MMIO_DEVICE_ID) != 1 ||
       *R(VIRTIO_MMIO_VENDOR_ID) != 0x554d4551){
        panic("Cannot find virtio network device");
    }
    //RESET
    *R(VIRTIO_MMIO_STATUS) |= status;

    //Acknowledge the device
    status |= VIRTIO_CONFIG_S_ACKNOWLEDGE;
    *R(VIRTIO_MMIO_STATUS) = status;

    //Acknowledge that we can drive the device
    status |= VIRTIO_CONFIG_S_DRIVER;
    *R(VIRTIO_MMIO_STATUS) = status;
    //Negotiate features
    uint32 features = *R(VIRTIO_MMIO_DEVICE_FEATURES);
	features &= ~(1 << VIRTIO_NET_F_CSUM);
	features = (1 << VIRTIO_F_NOTIFY_ON_EMPTY);
	features &= ~(1 << VIRTIO_NET_F_GUEST_CSUM);
	features &= ~(1 << VIRTIO_NET_F_GSO);
	features &= ~(1 << VIRTIO_NET_F_GUEST_TS04);
	features &= ~(1 << VIRTIO_NET_F_GUEST_TS06);
	features &= ~(1 << VIRTIO_NET_F_GUEST_ECN);
	features &= ~(1 << VIRTIO_NET_F_GUEST_UFO);
	features &= ~(1 << VIRTIO_NET_F_HOST_TS04);
	features &= ~(1 << VIRTIO_NET_F_HOST_TS06);
	features &= ~(1 << VIRTIO_NET_F_HOST_ECN);
	features &= ~(1 << VIRTIO_NET_F_HOST_UFO);
	#ifdef MERGE_BUFFERS
	features |= (1 << VIRTIO_NET_F_MRG_RXBUF);
	#else
	features &= ~(1 << VIRTIO_NET_F_MRG_RXBUF);
	#endif
	features &= ~(1 << VIRTIO_NET_F_STATUS);
	features |= (1 << VIRTIO_NET_F_CTRL_VQ);
	features &= ~(1 << VIRTIO_NET_F_CTRL_RX);
	features &= ~(1 << VIRTIO_NET_F_CTRL_VLAN);
	features &= ~(1 << VIRTIO_NET_F_GUEST_ANNOUNCE);
    features |= (1 << VIRTIO_NET_F_MAC);
    //Set features
    *R(VIRTIO_MMIO_DRIVER_FEATURES) = features;

	//Finished features
    status |= VIRTIO_CONFIG_S_FEATURES_OK;

    *R(VIRTIO_MMIO_STATUS) = status;

	uint32 check = *R(VIRTIO_MMIO_STATUS);

	if(!(check & VIRTIO_CONFIG_S_FEATURES_OK)){
		panic("NET DEVICE NOT NEGOTIATED FEATURES");
	}

    //Tell the driver what the page size is
    *R(VIRTIO_MMIO_GUEST_PAGE_SIZE) = PGSIZE;

	
    //Store device config
	volatile uint8 *pointer = (volatile uint8 *)(VIRTIO1 + VIRTIO_DEVICE_CONFIG_SPACE);
    for(uint8 i=0;i<6;i++){
		uint8 temp = *pointer;
		while(temp != *pointer){
			temp = *pointer;
		}
	    virtio_net_config.mac[i] = temp;
		mac_addr[i] = temp;
	    pointer++;
    }
    pointer = pointer + 6;
    virtio_net_config.status = *pointer;
    //Set the queue memory all to zeros
    memset(receiveq.pages, 0, 2*PGSIZE);

    //Setup the receive queue
    *R(VIRTIO_MMIO_QUEUE_SEL) = 0;
	if(*R(VIRTIO_MMIO_QUEUE_NUM_MAX) == 0){
		panic("virtio-net has no queue 0");
	}
	*R(VIRTIO_MMIO_QUEUE_ALIGN) = PGSIZE;
    //Set the number of descriptors
    *R(VIRTIO_MMIO_QUEUE_NUM) = VIRTIO_NET_QUEUE_SIZE;
    //Start of pages
    receiveq.desc = (struct virtq_desc *)receiveq.pages;
    //Add 1 descriptor
    receiveq.avail = (struct virtq_avail *)(receiveq.pages + VIRTIO_NET_QUEUE_SIZE * sizeof(struct virtq_desc));
    receiveq.used = (struct virtq_used *)(receiveq.pages + 4096);
    
    //Set queue address
    *R(VIRTIO_MMIO_QUEUE_PFN) = ((uint64)receiveq.pages) >> PGSHIFT;

	//*R(VIRTIO_MMIO_QUEUE_READY) = 1;

    //Transmit Queue
    memset(transmitq.pages,0, 2*PGSIZE);
    transmitq.desc = (struct virtq_desc *)transmitq.pages;
    transmitq.avail = (struct virtq_avail *)(transmitq.pages + VIRTIO_NET_QUEUE_SIZE * sizeof(struct virtq_desc));
    transmitq.used = (struct virtq_used *)(transmitq.pages + 4096);

    *R(VIRTIO_MMIO_QUEUE_SEL) = 1;
	if(*R(VIRTIO_MMIO_QUEUE_NUM_MAX) == 0){
		panic("virtio-net has no queue 1");
	}
    *R(VIRTIO_MMIO_QUEUE_NUM) = VIRTIO_NET_QUEUE_SIZE;

	*R(VIRTIO_MMIO_QUEUE_ALIGN) = PGSIZE;
    //Set the transmit queue address
    *R(VIRTIO_MMIO_QUEUE_PFN) = ((uint64)transmitq.pages) >> PGSHIFT;
	*R(VIRTIO_MMIO_QUEUE_READY) = 1;

    //Control Queue
    memset(controlq.pages,0, 2*PGSIZE);
    controlq.desc = (struct virtq_desc *)controlq.pages;
    controlq.avail = (struct virtq_avail *)(controlq.pages + VIRTIO_NET_QUEUE_SIZE * sizeof(struct virtq_desc));
    controlq.used = (struct virtq_used *)(controlq.pages + 4096);

    *R(VIRTIO_MMIO_QUEUE_SEL) = 2;
	if(*R(VIRTIO_MMIO_QUEUE_NUM_MAX) == 0){
		panic("virtio-net has no queue 2");
	}
    *R(VIRTIO_MMIO_QUEUE_NUM) = VIRTIO_NET_QUEUE_SIZE;

	*R(VIRTIO_MMIO_QUEUE_ALIGN) = PGSIZE;
    //Set the transmit queue address
    *R(VIRTIO_MMIO_QUEUE_PFN) = ((uint64)controlq.pages) >> PGSHIFT;




	
	//Fill the receive queue with buffers.
	fill_recv_queue();
	//Tell the driver its ready to go
    status |= VIRTIO_CONFIG_S_DRIVER_OK;
    *R(VIRTIO_MMIO_STATUS) = status;
    return 0;
}
//Function to create the header that preceeds the packet to be sent
struct virtio_net_hdr create_net_header(uint8 *packet, uint64 length){
	struct virtio_net_hdr hdr;
	hdr.flags = 0;
	hdr.hdr_len = 10;
	hdr.gso_type = 0;
	hdr.gso_size = 0;
	hdr.csum_start=0;
	hdr.csum_offset=0;
    hdr.num_buffers=1;
	return hdr;
}

int virtio_net_send(int n,const void *packet,int length){
	//Store address of packet
	uint32 *data = (uint32 *)packet;
	struct virtio_net_hdr hdr = create_net_header(packet,length);
	//Store address of buffer
	uint8 *buf = transmit_buffers[buffer_index].buf;
	memset(buf,0,BUF_SIZE);
	//copy packet and hdr to buffer
	memmove(buf,&hdr,sizeof(struct virtio_net_hdr));
	memmove(buf+sizeof(struct virtio_net_hdr),packet,length);
	//Place buffer in descriptor
	transmitq.desc[buffer_index].addr=buf;
	transmitq.desc[buffer_index].len=length+sizeof(struct virtio_net_hdr);
	transmitq.desc[buffer_index].flags=0;
	transmitq.desc[buffer_index].next=0;
	transmitq.avail->idx=transmitq.avail->idx + 1;
    //Update the available ring
	transmitq.avail->ring[buffer_index]=buffer_index;
	*R(VIRTIO_MMIO_QUEUE_NOTIFY)=1;
	buffer_index = (buffer_index + 1) % VIRTIO_NET_QUEUE_SIZE;
	return 0;
	//*R(VIRTIO_MMIO_INTERRUPT_ACK)=1;
	//add_buf_to_desc(buf,sizeof(struct virtio_net_hdr) + length,0,0);
} 
void fill_recv_queue(){
	unsigned char *buf;
	for(int i=0;i<VIRTIO_NET_QUEUE_SIZE;i++){
		//Get a new buffer
		buf=receive_buffers[i].buf;
		//Set buffer address in the descriptor
		receiveq.desc[i].addr = buf;
		//Set the length of the buffer
		receiveq.desc[i].len = BUF_SIZE;
		receiveq.desc[i].flags = 2;
		receiveq.desc[i].next = 0;
	}
	//Make the device aware of buffers
	receiveq.avail->idx = 1;
	receiveq.avail->ring[0]=0;
	*R(VIRTIO_MMIO_QUEUE_NOTIFY)=0;
}

int virtio_net_recv(int n,void *data,int length){
	//New packet arrived
	if(receiveq.used->idx > recv_packet_counter){
		int next_buffer;
		//Update packet counter
		recv_packet_counter=receiveq.used->idx;	
		//Select the receive queue
		*R(VIRTIO_MMIO_QUEUE_SEL)=0;
		uint8 *packet = (uint8*)data;
		next_buffer = receiveq.avail->ring[receiveq.avail->idx % VIRTIO_NET_QUEUE_SIZE];
		int len = receiveq.desc[next_buffer].len;
		//Read the virtio hdr
		struct virtio_net_hdr *hdr=receiveq.desc[next_buffer].addr;
		//Copy the packet to the pbuf
		memmove(packet,(receiveq.desc[next_buffer].addr)+sizeof(struct virtio_net_hdr),length);

		//Increment buffer counter
		next_buffer = (next_buffer+1) % VIRTIO_NET_QUEUE_SIZE;
		//Zero the buffer
		memset(receiveq.desc[next_buffer].addr,0,BUF_SIZE);
		//Reset the buffer length
		receiveq.desc[next_buffer].len = BUF_SIZE;
		//Tell the device about the next buffer available
		receiveq.avail->ring[receiveq.avail->idx % VIRTIO_NET_QUEUE_SIZE]=0;
		//Update the idx
		receiveq.avail->idx += 1;
		//Notify the queue
		*R(VIRTIO_MMIO_QUEUE_NOTIFY)=0;
		return len;
	}
	//No new packets
	else{
		return 0;
	}
	
}

//Handle network interrupt
void virtio_net_intr(){
	//printf("GOT TO HERE WOOP");
}
