//
// virtio device definitions.
// for both the mmio interface, and virtio descriptors.
// only tested with qemu.
// this is the "legacy" virtio interface.
//
// the virtio spec:
// https://docs.oasis-open.org/virtio/virtio/v1.1/virtio-v1.1.pdf
//

// virtio mmio control registers, mapped starting at 0x10001000.
// from qemu virtio_mmio.h
#define VIRTIO_MMIO_MAGIC_VALUE		0x000 // 0x74726976
#define VIRTIO_MMIO_VERSION		0x004 // version; 1 is legacy
#define VIRTIO_MMIO_DEVICE_ID		0x008 // device type; 1 is net, 2 is disk
#define VIRTIO_MMIO_VENDOR_ID		0x00c // 0x554d4551
#define VIRTIO_MMIO_DEVICE_FEATURES	0x010
#define VIRTIO_MMIO_DRIVER_FEATURES	0x020
#define VIRTIO_MMIO_GUEST_PAGE_SIZE	0x028 // page size for PFN, write-only
#define VIRTIO_MMIO_QUEUE_SEL		0x030 // select queue, write-only
#define VIRTIO_MMIO_QUEUE_NUM_MAX	0x034 // max size of current queue, read-only
#define VIRTIO_MMIO_QUEUE_NUM		0x038 // size of current queue, write-only
#define VIRTIO_MMIO_QUEUE_ALIGN		0x03c // used ring alignment, write-only
#define VIRTIO_MMIO_QUEUE_PFN		0x040 // physical page number for queue, read/write
#define VIRTIO_MMIO_QUEUE_READY		0x044 // ready bit
#define VIRTIO_MMIO_QUEUE_NOTIFY	0x050 // write-only
#define VIRTIO_MMIO_INTERRUPT_STATUS	0x060 // read-only
#define VIRTIO_MMIO_INTERRUPT_ACK	0x064 // write-only
#define VIRTIO_MMIO_STATUS		0x070 // read/write

// status register bits, from qemu virtio_config.h
#define VIRTIO_CONFIG_S_ACKNOWLEDGE	1
#define VIRTIO_CONFIG_S_DRIVER		2
#define VIRTIO_CONFIG_S_DRIVER_OK	4
#define VIRTIO_CONFIG_S_FEATURES_OK	8

// device feature bits
#define VIRTIO_BLK_F_RO              5	/* Disk is read-only */
#define VIRTIO_BLK_F_SCSI            7	/* Supports scsi command passthru */
#define VIRTIO_BLK_F_CONFIG_WCE     11	/* Writeback mode available in config */
#define VIRTIO_BLK_F_MQ             12	/* support more than one vq */
#define VIRTIO_F_ANY_LAYOUT         27
#define VIRTIO_RING_F_INDIRECT_DESC 28
#define VIRTIO_RING_F_EVENT_IDX     29

// net device feature bits
#define VIRTIO_NET_F_CSUM           0
#define VIRTIO_NET_F_GUEST_CSUM     1
#define VIRTIO_NET_F_MAC            5
#define VIRTIO_NET_F_GSO            6
#define VIRTIO_NET_F_GUEST_TS04     7
#define VIRTIO_NET_F_GUEST_TS06     8
#define VIRTIO_NET_F_GUEST_ECN      9
#define VIRTIO_NET_F_GUEST_UFO      10
#define VIRTIO_NET_F_HOST_TS04      11
#define VIRTIO_NET_F_HOST_TS06      12
#define VIRTIO_NET_F_HOST_ECN       13
#define VIRTIO_NET_F_HOST_UFO       14
#define VIRTIO_NET_F_MRG_RXBUF      15
#define VIRTIO_NET_F_STATUS         16
#define VIRTIO_NET_F_CTRL_VQ        17
#define VIRTIO_NET_F_CTRL_RX        18
#define VIRTIO_NET_F_CTRL_VLAN      19
#define VIRTIO_NET_F_GUEST_ANNOUNCE 21
#define VIRTIO_F_NOTIFY_ON_EMPTY    24
#define VIRTIO_DEVICE_CONFIG_SPACE 0x100
#define VIRTIO_QUEUE_DESC_LOW 0x080
#define VIRTIO_QUEUE_DESC_HIGH 0x084
#define VIRTIO_QUEUE_AVAIL_LOW 0x090
#define VIRTIO_QUEUE_AVAIL_HIGH 0x094
#define VIRTIO_QUEUE_USED_LOW 0x0a0
#define VIRTIO_QUEUE_USED_HIGH 0x0a4


// this many virtio descriptors.
// must be a power of two.
#define NUM 8

//Max size for page size 4096
#define VIRTIO_NET_QUEUE_SIZE 227

//Virtio_net_descriptor buffer size
#define BUF_SIZE 1600 

//Sets the mrg_rx feature bit and changes the virtio_net_hdr structure
#define MERGE_BUFFERS 1


// a single descriptor, from the spec.
struct virtq_desc {
  uint8 *addr;
  uint32 len;
  uint16 flags;
  uint16 next;
};
#define VRING_DESC_F_NEXT  1 // chained with another descriptor
#define VRING_DESC_F_WRITE 2 // device writes (vs read)

// the (entire) avail ring, from the spec.
struct virtq_avail {
  uint16 flags; // always zero
  uint16 idx;   // driver will write ring[idx] next
  uint16 ring[VIRTIO_NET_QUEUE_SIZE]; // descriptor numbers of chain heads
  uint16 unused;
};

// one entry in the "used" ring, with which the
// device tells the driver about completed requests.
struct virtq_used_elem {
  uint32 id;   // index of start of completed descriptor chain
  uint32 len;
};

struct virtq_used {
  uint16 flags; // always zero
  uint16 idx;   // device increments when it adds a ring[] entry
  struct virtq_used_elem ring[NUM];
};

// these are specific to virtio block devices, e.g. disks,
// described in Section 5.2 of the spec.

#define VIRTIO_BLK_T_IN  0 // read the disk
#define VIRTIO_BLK_T_OUT 1 // write the disk

// the format of the first descriptor in a disk request.
// to be followed by two more descriptors containing
// the block, and a one-byte status.
struct virtio_blk_req {
  uint32 type; // VIRTIO_BLK_T_IN or ..._OUT
  uint32 reserved;
  uint64 sector;
};


//HEADER for packets
struct virtio_net_hdr {
	#define VIRTIO_NET_HDR_F_NEEDS_CSUM 1
	uint8 flags;
	#define VIRTIO_NET_HDR_GSO_NONE 0
	uint8 gso_type;
	uint16 hdr_len;
	uint16 gso_size;
	uint16 csum_start;
	uint16 csum_offset;
#ifdef MERGE_BUFFERS
	uint16 num_buffers;
#endif
};

//Internal buffer structure
static struct virt_buffer {
    uint8 *buf[BUF_SIZE];
    uint8 used;
};


static struct virt_queue_net{
    //2 Pages of memory for the queue
    char pages[2*PGSIZE];

    struct virtq_desc *desc;

    struct virtq_avail *avail;

    struct virtq_used *used;
} __attribute__ ((aligned (PGSIZE))) ; 

struct virtio_net_ctrl {
	uint8 class;
	uint8 command;
	//uint8 commandspecificdata[];
	uint8 ack;
};
