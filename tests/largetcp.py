import socket
import time
import sys

host = socket.gethostname()
port = 2001                   # The same port as used by the server
count =0


packet_count = int(sys.argv[1])
packet_size = int(sys.argv[2])
test_count = int(sys.argv[3])
print("Starting...")
print("Sending " + sys.argv[1] + " packets")
print("Packet size: " + sys.argv[2])
print()
test=0
bytes_to_send = bytes(str("A")*packet_size, 'utf-8')
while(test < test_count):
	resultsFile = open("tcp-"+str(packet_count)+"-"+str(packet_size)+".txt",'a')
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((host, port))
	start_time=time.time()
	for i in range(packet_count):
		if(i%5000==0):
			print("Packets sent: " + str(i))
		s.sendall(bytes_to_send)
		data = s.recv(2048)
		if(data != bytes_to_send):
			print("Incorrect data")
	end_time=time.time()
	s.close()
	bytes_send = (packet_count * len(bytes_to_send))/1024
	speed = bytes_send/(end_time - start_time)
	print("Speed:"+str(speed)+"KB/S")
	resultsFile.write(str(int(speed))+"\n")
	time.sleep(5)
	test+=1
resultsFile.close()	
