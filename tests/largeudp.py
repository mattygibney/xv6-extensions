import socket
import time
import sys

host = socket.gethostname()
host = "127.0.0.1"
port = 2000                   # The same port as used by the server
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


#Gather parameters
packet_count = int(sys.argv[1])
packet_size = int(sys.argv[2])
test_count = int(sys.argv[3])
print("Starting...")
print("Sending " + sys.argv[1] + " packets")
print("Packet size: " + sys.argv[2])
print()

test = 0

#Generate data for packet
bytes_to_send = bytes(str("A")*packet_size,'utf-8')
while(test < test_count):
	#Open the results file
	resultsFile = open("udp-"+str(packet_count)+"-"+str(packet_size)+".txt",'a')
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	start_time=time.time()
	for i in range(packet_count):
		if(i % 5000 == 0 and i > 0 ):
			print("Packets sent: "+ str(i))
		s.sendto(bytes_to_send, (host,port))
		data,addr = s.recvfrom(1560)
		if(data != bytes_to_send):
			print("Packet dropped")

	end_time=time.time()
	bytes_send = (packet_count * len(bytes_to_send))/1024
	speed = bytes_send/(end_time - start_time)
	print("Speed:"+str(speed)+"KB/s")
	s.close()
	resultsFile.write(str(int(speed))+"\n")
	test += 1
resultsFile.close()
