#!/bin/bash

if [ $EUID -ne "0" ] ; then
	echo "Please run as root"
	exit 1
fi
sudo arping -I br0 ff:ff:ff:ff:ff:ff
