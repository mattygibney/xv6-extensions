#!/bin/bash
if [ $# -ne 2 ]; then
	echo "Please supply infile outfile"
	exit 1
fi
editcap -C 10 $1 $2
echo "virtio_net header removed from packets"
echo "Opening file with wireshark"
wireshark $2
rm -i $1
