#!/bin/bash
if [ $EUID -ne 0 ];then
	echo "Please run as root"
fi
brctl delif br0 tap0
tunctl -d tap0
brctl delif br0 enp0s3
ifconfig br0 down
brctl delbr br0
ifconfig enp0s3 up
dhclient -v enp0s3

