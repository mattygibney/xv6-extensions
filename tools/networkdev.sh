#!/bin/bash
if [ $EUID -ne 0 ];then
	echo "Please run as root"
	exit 1
fi
brctl addbr br0
ip addr flush dev enp0s3
brctl addif br0 enp0s3 
tunctl -t tap0 -u `whoami`
brctl addif br0 tap0
ifconfig tap0 hw ether cc:cc:cc:cc:cc:cc 
ifconfig enp0s3 up
ifconfig tap0 up
ifconfig br0 up

brctl show
dhclient -v br0

